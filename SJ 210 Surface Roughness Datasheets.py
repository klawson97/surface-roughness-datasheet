# -*- coding: utf-8 -*-
"""
Created on Fri Jun  4 13:14:48 2021

@author: klawson
"""

import pandas as pd
import xlsxwriter
import os

# Use the function below to find the five averages that the surface roughness tester measures
# You will need to input the folder location with all the desired files and what you want the name of the output file to be.
# Examples are shown below this function.


def organize_surftest_data(input_folder, output_file):

    filenames = []
    fullfilename = []
    
        #import all Excel files
    rafolder = r'M:\58-92 Salt Dep Corr Testing\Testing\Scoping\Surface Roughness Measurements\410SS'
    for (root, dirs, files) in os.walk(input_folder):
        for name in files:
            filenames.append(name)
            WorkingFile = os.path.join(root,name)
            fullfilename.append(WorkingFile)
            df_ra = pd.DataFrame(list(zip(fullfilename,filenames)), columns=['Full Directory Address','File Name'])
    #df_ra[["Measurement Orientation"]] = float('NaN')
    df_ra[["Ra 1 (micron)","Ra 2 (micron)","Ra 3 (micron)", "Ra 4 (micron)", "Ra 5 (micron)","Avg Ra (micron)","St.Dev. (micron)"]] = float('NaN')
          
    for index, row in df_ra.iterrows():
        xl = pd.ExcelFile(df_ra['Full Directory Address'].loc[index])
        
        # #call out the orientation of the measurement
        # if 'Horizontal' in row['File Name']:
        #     df_ra['Measurement Orientation'].loc[index] == "Horizontal"
        # if 'Vertical' in row['File Name']:
        #     df_ra['Measurement Orientation'].loc[index] == "Vertical"
    
        #pull the 6th column with the roughness measurements
    
        df = xl.parse("DATA", header=None)
        numpoints = int(len(df.iloc[:,5])/5)
        numindex = numpoints-1
    
        #columns with Ra values
        df_ra['Avg Ra (micron)'].loc[index] = df.iloc[:,5].abs().mean()
        df_ra['Ra 1 (micron)'].loc[index] = (df.iloc[0:numindex,5].abs().sum())/numpoints
        df_ra['Ra 2 (micron)'].loc[index] = (df.iloc[numpoints:(numpoints*2)-1,5].abs().sum())/numpoints
        df_ra['Ra 3 (micron)'].loc[index] = (df.iloc[(numpoints*2):(numpoints*3)-1,5].abs().sum())/numpoints
        df_ra['Ra 4 (micron)'].loc[index] = (df.iloc[(numpoints*3):(numpoints*4)-1,5].abs().sum())/numpoints
        df_ra['Ra 5 (micron)'].loc[index] = (df.iloc[(numpoints*4):(numpoints*5)-1,5].abs().sum())/numpoints
        df_ra['St.Dev. (micron)'].loc[index] = df_ra.iloc[index,2:7].std()
    # save output
    p_file = input_folder +'/'+ output_file
    writer = pd.ExcelWriter(p_file, engine='xlsxwriter')
    df_ra.to_excel(writer, sheet_name='Ra Data')
    writer.save()

    return df_ra

rafolder = 'M:/58-92 Salt Dep Corr Testing/Testing/Scoping/Surface Roughness Measurements/M152'
rafile = 'M152 Surface Roughness.xlsx'
organize_surftest_data(rafolder, rafile)


rafolder = 'M:/58-92 Salt Dep Corr Testing/Testing/Scoping/Surface Roughness Measurements/410SS'
rafile = '410SS Surface Roughness.xlsx'
organize_surftest_data(rafolder, rafile)